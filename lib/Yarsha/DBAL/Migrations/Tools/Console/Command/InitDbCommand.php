<?php


namespace Yarsha\DBAL\Migrations\Tools\Console\Command;

use Symfony\Component\Console\Input\ArrayInput;

use Doctrine\DBAL\Types\Type;

use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Table;

use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputInterface;

use Symfony\Component\Console\Input\InputArgument;

use Symfony\Component\Console\Command\Command;

class InitDbCommand extends Command
{
	protected function configure(){
		$this->ignoreValidationErrors();
		
		$this
		->setName('db:init')
		->setDefinition(array(
				new InputArgument('command_name', InputArgument::OPTIONAL, 'The command name', 'help')
		))
		->setDescription('Initializes database for a project')
		->setHelp(<<<EOF
The <info>%command.name%</info> command displays help for a given command:
		
  <info>php %command.full_name% list</info>
		
You can also output the help as XML by using the <comment>--xml</comment> option:
		
  <info>php %command.full_name% --xml list</info>
		
To display the list of available commands, please use the <info>list</info> command.
EOF
		)
		;
	}
	
	
	protected function execute(InputInterface $input, OutputInterface $output){
		$output->writeln("Initializing Database...");
		
		$em = $this->getHelper('em')->getEntityManager();
		$connection = $em->getConnection();
		
		$output->writeln("Creating options table.");
		
		if ( ! $connection->getSchemaManager()->tablesExist('f1_options')) {
			$columns = array(
					'id'			=>	new Column('id',Type::getType('integer'),array('length' => 11)),
					'option_name'	=>	new Column('option_name', Type::getType('string'), array('length' => 255)),
					'option_value'	=>	new Column('option_value', Type::getType('text')),
					'autoload'		=>	new Column('autoload', Type::getType('boolean'),array('default'	=>	TRUE))
			);
			$table = new Table('f1_options', $columns);
			$table->setPrimaryKey(array('id'));
			$table->addUniqueIndex(array('option_name'));
			
			$connection->getSchemaManager()->createTable($table);
			
			$output->writeln("Options table created.");
		}else{
			$output->writeln("Options table already exists.");
		}
		
		
		$output->writeln("Creating sessions table.");
		if ( ! $connection->getSchemaManager()->tablesExist('f1_sessions')) {
			$columns = array(
					'session_id'	=>	new Column('session_id',Type::getType('string'),array('length' => 255)),
					'ip_address'	=>	new Column('ip_address', Type::getType('string'), array('length' => 255)),
					'user_agent'	=>	new Column('user_agent', Type::getType('string')),
					'last_activity'	=>	new Column('last_activity', Type::getType('integer'),array('length'	=>	11)),
					'user_data'		=>	new Column('user_data', Type::getType('text'))
			);
			$table = new Table('f1_sessions', $columns);
			$table->setPrimaryKey(array('session_id'));
				
			$connection->getSchemaManager()->createTable($table);
				
			$output->writeln("Sesions table created.");
		}else{
			$output->writeln("Sesions table already exists.");
		}
		
		$output->writeln("Creating a diff.");
		
		$command = $this->getApplication()->find('migrations:diff');
	
		$returnCode = $command->run($input, $output);
		
		$output->writeln("Diff Created.");
		$output->writeln("Migrating Database to latest version.");
		
		$this->getApplication()->find('migrations:migrate')->run($input, $output);
		$output->writeln("Database migrated to latest version.");
	}
}
