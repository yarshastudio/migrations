<?php

namespace Yarsha\DBAL\Migrations\Tools\Console\Command;

use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputInterface,
	Doctrine\ORM\Tools\SchemaTool,
	Doctrine\DBAL\Version as DbalVersion;

use Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand;

class CustomDiffCommand extends DiffCommand
{
	public function execute(InputInterface $input, OutputInterface $output)
	{
		$isDbalOld = (DbalVersion::compare('2.2.0') > 0);
		$configuration = $this->getMigrationConfiguration($input, $output);
	
		$em = $this->getHelper('em')->getEntityManager();
		$conn = $em->getConnection();
		$platform = $conn->getDatabasePlatform();
		$metadata = $em->getMetadataFactory()->getAllMetadata();
	
		if (empty($metadata)) {
			$output->writeln('No mapping information to process.', 'ERROR');
	
			return;
		}
	
		if ($filterExpr = $input->getOption('filter-expression')) {
			if ($isDbalOld) {
				throw new \InvalidArgumentException('The "--filter-expression" option can only be used as of Doctrine DBAL 2.2');
			}
	
			$conn->getConfiguration()
			->setFilterSchemaAssetsExpression($filterExpr);
		}
	
		$tool = new SchemaTool($em);
	
		$fromSchema = $conn->getSchemaManager()->createSchema();
		$toSchema = $tool->getSchemaFromMetadata($metadata);
	
		//Not using value from options, because filters can be set from config.yml
		if ( ! $isDbalOld && $filterExpr = $conn->getConfiguration()->getFilterSchemaAssetsExpression()) {
			$tableNames = $toSchema->getTableNames();
			foreach ($tableNames as $tableName) {
				$tableName = substr($tableName, strpos($tableName, '.') + 1);
				if ( ! preg_match($filterExpr, $tableName)) {
					$toSchema->dropTable($tableName);
				}
			}
		}
	
		$fromSchema->dropTable('ys_sessions');
		$fromSchema->dropTable('ys_options');
	
		$up = $this->buildCodeFromSql($configuration, $fromSchema->getMigrateToSql($toSchema, $platform));
		$down = $this->buildCodeFromSql($configuration, $fromSchema->getMigrateFromSql($toSchema, $platform));
	
		if ( ! $up && ! $down) {
			$output->writeln('No changes detected in your mapping information.', 'ERROR');
	
			return;
		}
	
		$version = date('YmdHis');
		$path = $this->generateMigration($configuration, $input, $version, $up, $down);
	
		$output->writeln(sprintf('Generated new migration class to "<info>%s</info>" from schema differences.', $path));
	}
	
	private function buildCodeFromSql($configuration, array $sql)
	{
		$currentPlatform = $configuration->getConnection()->getDatabasePlatform()->getName();
		$code = array(
				"\$this->abortIf(\$this->connection->getDatabasePlatform()->getName() != \"$currentPlatform\", \"Migration can only be executed safely on '$currentPlatform'.\");", "",
		);
		foreach ($sql as $query) {
			if (strpos($query, $configuration->getMigrationsTableName()) !== false) {
				continue;
			}
			$code[] = "\$this->addSql(\"$query\");";
		}
	
		return implode("\n", $code);
	}
}