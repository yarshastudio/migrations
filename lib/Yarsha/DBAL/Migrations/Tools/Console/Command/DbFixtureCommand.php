<?php


namespace Yarsha\DBAL\Migrations\Tools\Console\Command;

use Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand;

use Symfony\Component\Console\Input\ArrayInput;

use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputInterface;

use Symfony\Component\Console\Input\InputArgument;

use Symfony\Component\Console\Command\Command;

use Doctrine\Common\DataFixtures\Loader;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;

class DbFixtureCommand extends Command
{
	protected function configure(){
		$this->ignoreValidationErrors();
		
		$this
		->setName('db:fix')
		->setDefinition(array(
				new InputArgument('fixture_name', InputArgument::REQUIRED, 'The fixture name')
		))
		->setDescription('Executes a db fixture')
		->setHelp("No help available");
	}
	
	protected function execute(InputInterface $input, OutputInterface $output){
		
		$fixture = $name = $input->getArgument('fixture_name');
		$em = $this->getHelper('em')->getEntityManager();
		
		$output->writeln("Loading fixture $fixture");
		
		$loader = new Loader();
		
		$loader->addFixture(new $fixture);
		
		$executor = new ORMExecutor($em);
		
		$executor->execute($loader->getFixtures(), TRUE);
		
	}
}